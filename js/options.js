var bgPage;


function saveAll(){
	localStorage.crucible_url = $('#crucible_url').val();
	bgPage.crucible_url = localStorage.crucible_url;
	localStorage.crucible_auth = $('#crucible_auth').val();
	bgPage.crucible_auth = localStorage.crucible_auth;
	bgPage.poller.getReviewsList('toReview');
	bgPage.poller.getReviewsList('toSummarize');
	bgPage.poller.getReviewsList('outForReview');
	bgPage.util.scheduleRequest();
}

function getFEAUTH(){
	login = $('#crucible_login').val();
	password = $('#crucible_password').val();
	crucible_url = $('#crucible_url').val();

	// URL to use for the request
	url = crucible_url + '/rest-service/auth-v1/login.json?userName=' + login + '&password=' + password;

	// xhr Request
	var xhr = new XMLHttpRequest();
	xhr.open("GET", url, true);
	$('#form_message').attr('class', 'info');
	$('#form_message').text('Sending request to the server...');
	$('#form_message').fadeIn();
	xhr.onreadystatechange = function() {
		if (xhr.status == 0){
			$('#form_message').attr('class', 'error');
			$('#form_message').text('The server could not be reached').delay(2000).fadeOut();
		}
		if (xhr.readyState == 4) {
			// JSON.parse does not evaluate the attacker's scripts.
			data = JSON.parse(xhr.responseText);
			$('#crucible_auth').val(data.token);
			$('#feauth_span').text(data.token);
			$('#url_span').text(crucible_url);
			var msg;
			switch (xhr.status){
				case 200:
					saveAll();
					msg = 'Your key has been saved!';
					iclass = 'success';
					break
				case 403:
					msg = 'Wrong auth params!';
					iclass = 'error'
					break
			}
			$('#form_message').attr('class', iclass);
			$('#form_message').text(msg).delay(2000).fadeOut();
			
			
		}
		
	}
	xhr.send();
}

function initUI() {

	if (localStorage.crucible_url) {
		$('#crucible_url').val(localStorage.crucible_url);
		$('#url_span').text(localStorage.crucible_url);
	} else {
		$('#crucible_url').val(bgPage.crucible_url);
		$('#url_span').text(bgPage.crucible_url);
	}
	if (localStorage.crucible_auth) {
		$('#crucible_auth').val(localStorage.crucible_auth);
		$('#feauth_span').text(localStorage.crucible_auth);
	} else {
		$('#crucible_auth').val(bgPage.crucible_auth);
		$('#feauth_span').text(bgPage.crucible_auth);

	}
}

$(function(){
	bgPage = chrome.extension.getBackgroundPage();
	initUI();
	$('#key_gen_form').submit(function(){
		getFEAUTH();
		return false;
	});
});